const std = @import("std");

const res_yes = [_][]const u8{
    "It is certain",
    "It is decidedly so",
    "Without a doubt",
    "Yes definitely",
    "You may rely on it",
    "As I see it, yes",
    "Most likely",
    "Outlook good",
    "Yes",
    "Signs point to yes",
};

const res_maybe = [_][]const u8{
    "Reply hazy, try again",
    "Ask again later",
    "Better not tell you now",
    "Cannot predict now",
    "Concentrate and ask again",
};

const res_no = [_][]const u8{
    "Don’t count on it",
    "My reply is no",
    "My sources say no",
    "Outlook not so good",
    "Very doubtful",
};

var r: std.rand.Xoshiro256 = undefined;

extern fn display([*]const u8, length: u32) void;

export fn init(seed: u64) void {
    r = std.rand.Xoshiro256.init(seed);
}

export fn do8ball() void {
    const which = r.random().intRangeLessThan(usize, 0, 3);
    var solution: []const u8 = undefined;
    if (which == 0) {
        solution = res_yes[r.random().intRangeLessThan(usize, 0, res_yes.len)];
    } else if (which == 1) {
        solution = res_maybe[r.random().intRangeLessThan(usize, 0, res_maybe.len)];
    } else if (which == 2) {
        solution = res_no[r.random().intRangeLessThan(usize, 0, res_no.len)];
    } else {
        return;
    }
    display(solution.ptr, @intCast(u32, solution.len));
}
