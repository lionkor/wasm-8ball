
.PHONY: all

all: www

main.wasm: src/main.zig
	zig build-lib src/main.zig -target wasm32-wasi -dynamic -O ReleaseSmall
	rm *.o -v

www: web/index.html main.wasm
	mkdir -p www
	cat web/index.html | sed 's/    /\t/g' | tr -d \\t | tr -d \\n | sed -E 's/\s+/ /g' >www/index.html
	mv main.wasm www/main.wasm -v

